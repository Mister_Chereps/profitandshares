﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ProfitAndShares.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ProfitAndSharesViewModel();
        }

        private void Control_Values(object sender, TextChangedEventArgs e)
        {
            var value = (TextBox)sender;
            if (value.Text != "")
            {
                try
                {
                    for (int i = 0; i < value.Text.Length; i++)
                    {
                        if ((!Char.IsNumber(value.Text[i]) && value.Text[i] != Convert.ToChar(".")) || value.Text[0] == Convert.ToChar("0"))
                        {
                            MessageBox.Show("Числовые значения должны быть большие нуля. Для действительных чисел использовать точку как разделитель.", "Ошибка.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void information_app_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Приложение способно вычислить полную годовую доходность акции и текущую цену акции на конец года по введенным пользователем, в доступные поля, начальным данным. Необходимо ввести:\n   Номинальную стоимость акции в руб.\n   Сумму дивидентов на акцию в руб.\n   Значение, на сколько эта сумма меньше, чем в прошлом году в %\n   Ставку ссудного процента на конец года в %\n   Значение, на сколько меньше её значения в началле года в %\nПрограмма сама выдаст итог по расчетам. Отчет можно сохранить в виде изображения.", "Инуструкция к приложению.");
        }

        private void report_img_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(1280, 720, 0, 0, PixelFormats.Pbgra32);
            renderTargetBitmap.Render(window_menu);
            PngBitmapEncoder pngImage = new PngBitmapEncoder();
            pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

            if (file_path.Text[file_path.Text.Length - 1] == Convert.ToChar("\\"))
            {
                file_path.Text = file_path.Text.TrimEnd(Convert.ToChar("\\")) + "\\";
            }
            else if (file_path.Text[file_path.Text.Length - 1] != Convert.ToChar("\\"))
            {
                file_path.Text = file_path.Text + "\\";
            }

            try
            {
                using (Stream fileStream = File.Create(file_path.Text + "Отчет_по_программе.png"))
                {
                    pngImage.Save(fileStream);
                }
                MessageBox.Show("Отчет сохранён.\n Путь к файлу: " + file_path.Text + "Отчет_по_программе.png", "Сохранено.");
            }
            catch
            {
                MessageBox.Show("Был задан неверный путь к папке.", "Не удалось сохранить отчет.");
            }
        }

        private void file_path_GotFocus(object sender, RoutedEventArgs e)
        {

        }
    }
}
