﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ProfitAndShares.Wpf
{
    class ProfitAndSharesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public ProfitAndSharesViewModel()
        {
            nominal = 100;
            dividend = 18;
            less_devidend = 10;
            bet_loan_percent = 12;
            less_bet_loan_percent = 12.5;
        }

        private double nominal;
        private double dividend;
        private double less_devidend;
        private double bet_loan_percent;
        private double less_bet_loan_percent;

        public double Nominal
        {
            get { return nominal; }

            set
            {
                nominal = value;
                OnPropertyChanged("Nominal");
                OnPropertyChanged("Result");
            }
        }
        public double Dividend
        {
            get { return dividend; }

            set
            {
                dividend = value;
                OnPropertyChanged("Dividend");
                OnPropertyChanged("Result");
            }
        }

        public double Less_devidend
        {
            get { return less_devidend; }

            set
            {
                less_devidend = value;
                OnPropertyChanged("Less_devidend");
                OnPropertyChanged("Result");
            }
        }

        public double Bet_loan_percent
        {
            get { return bet_loan_percent; }

            set
            {
                bet_loan_percent = value;
                OnPropertyChanged("Bet_loan_percent");
                OnPropertyChanged("Result");
            }
        }

        public double Less_bet_loan_percent
        {
            get { return less_bet_loan_percent; }

            set
            {
                less_bet_loan_percent = value;
                OnPropertyChanged("Less_bet_loan_percent");
                OnPropertyChanged("Result");
            }
        }

        public Tuple<double, double> Result
        {
            get { return new ProfitAndShares.Library.ProfitAndShares().MainMethod(nominal, dividend, less_devidend, bet_loan_percent, less_bet_loan_percent); }
        }
    }
}
