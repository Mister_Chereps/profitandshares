﻿using System;

namespace ProfitAndShares.Library
{
    public class ProfitAndShares
    {
        public Tuple<double, double> MainMethod(double nominal, double dividend, double less_devidend, double bet_loan_percent, double less_bet_loan_percent)
        {
            if (nominal < 0 || dividend < 0 || less_devidend < 0 || bet_loan_percent < 0 || less_bet_loan_percent < 0)
                throw new ArgumentException("Один из аргументов меньше нуля.");
            //годовые дивиденты на начало и конец года
            double dividend_begin = YearDividendBet(nominal, dividend, less_devidend);
            double dividend_end = YearDividendBet(nominal, dividend);
            //курсы акции на начало и конец года
            double rates_begin = StockRates(dividend_begin, bet_loan_percent, less_bet_loan_percent);
            double rates_end = StockRates(dividend_end, bet_loan_percent);
            //полная годовая доходность акции
            double full_profitability = FullYearProfitabilityStock(dividend_end, rates_end, rates_begin);
            //текущая цена акции на конец года
            double current_price = CurrentPriceStock(nominal, rates_end);

            return Tuple.Create(full_profitability, current_price);
        }

        //рассчет годового дивидента
        public double YearDividendBet(double nominal, double dividend, double less_devidend = 0)
        {
            if (nominal < 0 || dividend < 0 || less_devidend < 0)
                throw new ArgumentException("Номинал, дивидент или процентное значение меньше нуля.");
            return Math.Round(dividend * (1 + less_devidend / 100) / nominal, 3);
        }

        //рассчет курсов акции
        public double StockRates(double dividend, double bet_loan_percent, double less_bet_loan_percent = 0)
        {
            if (dividend < 0 || bet_loan_percent < 0 || less_bet_loan_percent < 0)
                throw new ArgumentException("Дивидент или процентные значения меньше нуля.");
            return Math.Round(dividend / (bet_loan_percent / 100 * (1 + less_bet_loan_percent / 100)), 2);
        }

        //рассчет полной годовой доходности акции
        public double FullYearProfitabilityStock(double dividend_end, double rates_end, double rates_begin)
        {
            return Math.Round((dividend_end + rates_end - rates_begin) * 100 / rates_begin, 2);
        }

        //рассчет текущей цены акции на конец года
        public double CurrentPriceStock(double nominal, double rates_end)
        {
            return Math.Round(nominal * rates_end, 2);
        }
    }
}
