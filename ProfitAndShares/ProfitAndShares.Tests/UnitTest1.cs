using System;
using NUnit.Framework;

namespace ProfitAndShares.Tests
{
    public class Tests
    {
        [Test]
        public void Test_Main()
        {
            double nominal = 100;
            double dividend = 18;
            double less_devidend = 10;
            double bet_loan_percent = 12;
            double less_bet_loan_percent = 12.5;
            var expected_result = (14.29, 150.0);
            var real_result = new ProfitAndShares.Library.ProfitAndShares().MainMethod(nominal, dividend, less_devidend, bet_loan_percent, less_bet_loan_percent);
            Assert.AreEqual(expected_result.Item1, real_result.Item1, 0.05);
            Assert.AreEqual(expected_result.Item2, real_result.Item2, 0.05);
        }

        [Test]
        public void Test_YearDividendBet()
        {
            double nominal = 100;
            double dividend = 18;
            //double less_devidend = 10;
            double expected_result = 0.18 /*0.198*/;
            double real_result = new ProfitAndShares.Library.ProfitAndShares().YearDividendBet(nominal, dividend/*, less_devidend*/);
            Assert.AreEqual(expected_result, real_result, 0.05);
        }

        [Test]
        public void Test_StockRates()
        {
            double dividend = 0.18 /*0.198*/;
            double bet_loan_percent = 12;
            //double less_bet_loan_percent = 12.5;
            double expected_result = 1.5 /*1.47*/;
            double real_result = new ProfitAndShares.Library.ProfitAndShares().StockRates(dividend, bet_loan_percent/*, less_bet_loan_percent*/);
            Assert.AreEqual(expected_result, real_result, 0.05);
        }

        [Test]
        public void Test_FullYearProfitabilityStock()
        {
            double dividend_end = 0.18;
            double rates_end = 1.5;
            double rates_begin = 1.47;
            double expected_result = 14.29;
            double real_result = new ProfitAndShares.Library.ProfitAndShares().FullYearProfitabilityStock(dividend_end, rates_end, rates_begin);
            Assert.AreEqual(expected_result, real_result, 0.05);
        }

        [Test]
        public void Test_CurrentPriceStock()
        {
            double nominal = 100;
            double rates_end = 1.5;
            double expected_result = 150;
            double real_result = new ProfitAndShares.Library.ProfitAndShares().CurrentPriceStock(nominal ,rates_end);
            Assert.AreEqual(expected_result, real_result, 0.05);
        }
    }
}